class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  include TimelistHelper

  
  private
  
    def only_logged_in
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to root_path
      end
    end
    
    def only_admin
      redirect_to root_path unless current_user.admin?
    end
end