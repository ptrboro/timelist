class WorkRecordsController < ApplicationController
    
    before_action :only_logged_in
    before_action :check_authorization, only: [:destroy]
    before_action :only_admin, only: [:update]
    
    def create
        newRecord = current_user.admin ? WorkRecord.new(record_params) : current_user.work_records.build(record_params)
            
        if newRecord.save
            flash[:success] = "Record added!"
        else
            flash[:danger] = "Record Error!"
        end
        
        redirect_back fallback_location: root_path
    end
    
    def destroy
        WorkRecord.find(params[:id]).update_attribute(:deleted, true)
        flash[:success] = "Record deleted"
        redirect_to root_url
    end
    
    def update
       record = WorkRecord.find(params[:id])
        if record.update_attributes(record_params)
          flash[:success] = "Record updated"
          redirect_back fallback_location: root_path
        else
          flash[:error] = "Record updating error"
          redirect_back fallback_location: root_path
        end
    end
    
    def index
       redirect_to root_url 
    end
    
    
    
    private
        def record_params

            if current_user.admin?
                params.require(:work_record).permit(:user_id,
                                               :time_start, :time_end, 
                                               :project_id,
                                               :description, :accepted) 
            else
                bufor = params.require(:work_record).permit(:time_start, :time_end, 
                                               :project_id,
                                               :description) 
                                               
                bufor[:time_start] = bufor[:time_start].to_datetime
                bufor[:time_end] = bufor[:time_end].to_datetime
                bufor
            end
        end
        
        def check_authorization
          user = WorkRecord.find(params[:id]).user
          redirect_to root_path unless current_user?(user) || current_user.admin?
        end
end
