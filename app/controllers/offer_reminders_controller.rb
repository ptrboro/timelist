class OfferRemindersController < ApplicationController
    
    before_action :only_logged_in
    before_action :only_admin
    
    def create
        @offer = Offer.find(params[:offer_reminder][:offer_id])
        @newReminder = @offer.reminders.build(reminder_params)
        if @newReminder.save
            flash[:success] = "Reminder created!"
            redirect_to @offer
        else
            @offer = Offer.find(params[:offer_reminder][:offer_id])
            @admins = User.where(admin: true)
            @clients = Client.order(name: :asc)
            @projects = Project.order(name: :asc)
            @newContact = ContactPerson.new
            render 'offers/show'
        end
    end
    
    def destroy
        @reminder = OfferReminder.find(params[:id])
        if @reminder.update_attribute(:deleted, true)
            flash[:success] = "Reminder deleted"
            redirect_to @reminder.offer
        else
           flash[:error] = "Something went wrong"
           redirect_to @reminder.offer
        end
    end
    
    private
        def reminder_params
           params.require(:offer_reminder).permit(:message, :date, :email, :offer_id) 
        end
end
