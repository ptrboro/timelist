class ContactPeopleController < ApplicationController

    before_action :only_logged_in
    before_action :only_admin
    
    def create
        if params[:contact_relation][:contact_person_id] == 'only_person'
             #utworz tylko osobe
             @client = Client.find(params[:contact_person][:client_id])
             @newContact = @client.contact_people.build(contact_params)
             if @newContact.save
                 flash[:success] = "Contact added"
                 redirect_to @client
             else
                 render_client(@client)
             end
        elsif params[:contact_relation][:contact_person_id] == 'new'
         #utworz osobe i relacje
         @offer = Offer.find(params[:contact_relation][:offer_id])
         @newContact = @offer.contact_people.build(contact_params)
            if @newContact.save
                @newContact.reload
                relation = @offer.contacts.build(contact_person_id: @newContact.id)
                if relation.save
                    flash[:success] = "Contact person added!"
                    redirect_to @offer
                else
                    flash[:warning] = "Something went wrong!"
                    redirect_to @offer
                end
            else
                render_offer(@offer)
            end
        else
         #utworz tylko relacje 
            @offer = Offer.find(params[:contact_relation][:offer_id])
            contact = @offer.contacts.build(contact_rel_params)
            if contact.save
                flash[:success] = "Contact person added!"
                redirect_to @offer
            else
                @newContact = ContactPerson.new
                render_offer(@offer)
                
            end
        end
    end
    
    
    
    
    def destroy
        contact = ContactPerson.find(params[:id])
        if contact.update_attribute(:deleted, true)
            flash[:success] = "Contact deleted"
            redirect_to contact.client
        else
            flash[:warning] = "Something went wrong"
            redirect_to contact.client
        end
    end

    private
        def contact_params
           params.require(:contact_person).permit(:name,
                                                  :client_id,
                                                  :tel,
                                                  :email,
                                                  :occupation,
                                                  :description) 
        end
        
        def contact_rel_params
           params.require(:contact_relation).permit(:contact_person_id) 
        end
        
        def render_client(client)
            @newProject = Project.new
            @projects = @client.projects.order(:name).paginate(page: params[:page])
            @newContactRel = ContactRelation.new
            render 'clients/show'
        end
        
        def render_offer(offer)
            @admins = User.where(admin: true)
            @clients = Client.order(name: :asc)
            @projects = Project.order(name: :asc)
            @newReminder = OfferReminder.new
            @newContactRel = ContactRelation.new
            render 'offers/show'
        end
        
end
