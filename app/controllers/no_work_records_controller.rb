class NoWorkRecordsController < ApplicationController
    
    def create
        newRecord = current_user.admin ? NoWorkRecord.new(record_params) : current_user.no_work_records.build(record_params)
        newRecord.non_work_type_id = 1 if params[:no_work_record][:record_type] == "ferie"
            
        if newRecord.save
            flash[:success] = "Ferie request sent!"
        else
            flash[:danger] = "Ferie request Error!"
        end
        
        redirect_to root_url
    end
    
    private
        def record_params
            if current_user.admin?
                params.require(:no_work_record).permit(:user_id,
                                               :non_work_type_id,
                                               :time_start, :time_end, 
                                               :description, :accepted) 
            else
                bufor = params.require(:no_work_record).permit(:time_start, :time_end, 
                                               :non_work_type_id,
                                               :description) 
                bufor[:time_start] = bufor[:time_start].to_datetime
                bufor[:time_end] = bufor[:time_end].to_datetime
                bufor
            end
        end
    
end
