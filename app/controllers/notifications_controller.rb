class NotificationsController < ApplicationController
    
    before_action :only_logged_in
    before_action :check_authorization
    
    def show
        notification = Notification.find(params[:id])
        notification.update_attribute(:seen, true)
        redirect_to send("#{notification.content_type}_path", notification.content_id)
    end
    
    private
        def check_authorization
            notification = Notification.find(params[:id])
            redirect_to root_path unless current_user?(notification.user)
        end
end
