class UsersController < ApplicationController
  
  before_action :only_logged_in
  before_action :check_authorization, only: [:show, :update]
  before_action :only_admin, only: [:create, :index, :destroy]
  
  def show
    @user = User.find_by(id: params[:id])
  end
  
  def index
    @users = User.order(:name).paginate(page: params[:page])
    @newUser = User.new
    @user = User.find_by(id: params[:edit_user]) if params[:edit_user]
  end
  
  def create
    @newUser = User.new(user_params)
    @newUser.password = @newUser.password_confirmation = first_password
    if @newUser.save
      flash[:success] = "New user successfully created"
      redirect_to @newUser
    else
      @users = User.order(:name).paginate(page: params[:page])
      render 'index'
    end
  end
  
  def destroy
    User.find(params[:id]).update_attribute(:deleted, true)
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      @user.reload
      render 'show'
    end
  end
  
  private
    def user_params
      if current_user.admin?
        params.require(:user).permit(:name, :email, :address, :occupation, :password, :password_confirmation, :admin)
      else
        params.require(:user).permit(:name, :email, :address, :occupation, :password, :password_confirmation)
      end
    end
    
    def first_password
      SecureRandom.urlsafe_base64(6)
    end
    
    def check_authorization
      @user = User.find(params[:id])
      redirect_to root_path unless current_user?(@user) || current_user.admin?
    end
end
