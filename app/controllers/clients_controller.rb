class ClientsController < ApplicationController
    
    before_action :only_logged_in
    before_action :only_admin
    
    def index
       @clients = Client.order(:name).paginate(page: params[:page])
       @newClient = Client.new
    end
    
    def show
        if @client = Client.find_by(id: params[:id])
            @newProject = Project.new
            @projects = @client.projects.order(:name).paginate(page: params[:page])
            @newContact = ContactPerson.new
            @newContactRel = ContactRelation.new
        else
            redirect_back fallback_location: root_path
        end
    end
    
    def create
        @clients = Client.order(:name).paginate(page: params[:page])
        @newClient = Client.new(client_params)
        if @newClient.save
            flash[:success] = "Client created!"
            redirect_to @newClient
        else
            render 'clients/index'
        end
    end
    
    def update
        if @client.update_attributes(client_params)
          flash[:success] = "Client updated"
          redirect_to @client
        else
          @client.reload
          @newProject = Project.new
          @projects = @client.projects.order(:name).paginate(page: params[:page])
          @newContact = ContactPerson.new
          @newContactRel = ContactRelation.new
          render 'show'
        end
    end
    
    def destroy
        client = Client.find(params[:id])
        client.update_attribute(:deleted, true)
        client.projects.update_all(deleted: true)
        client.offers.update_all(deleted: true)
        client.contact_people.update_all(deleted: true)
        flash[:success] = "Client deleted"
        redirect_to clients_url
    end
    
    private
        def client_params
            params.require(:client).permit(:name, :address)
        end
    
end
