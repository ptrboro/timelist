class ProjectsController < ApplicationController
  
  before_action :only_logged_in
  before_action :only_admin
  
  def index
    @projects = Project.paginate(page: params[:page])
    @newProject = Project.new
    @clients = Client.order(name: :asc)
  end

  def show
    @project = Project.find_by(id: params[:id])
    redirect_to projects_path unless Project.exists?(@project)
    @newOffer = Offer.new
    @admins = User.where(admin: true)
    
    
    default_start = (Date.today + 7.hours)
    default_end = (Date.today + 15.hours)
    timelist_params
    @newRecord = WorkRecord.new(time_start: default_start, time_end: default_end)
    @records = TimelistRecord.timelist(@from, @to, @user, params[:id], false, @notconf).paginate(page: params[:page], per_page: 15).order(time_start: :desc, created_at: :desc)
  end

  def create
    @clients = Client.order(name: :asc)
    @projects = Project.paginate(page: params[:page])
    @newProject = Project.new(project_params)
    if @newProject.save
      flash[:success] = "Project created!"
      redirect_to @newProject
    else
      render 'index'
    end
  end

  def update
    @project = Project.find(params[:id])
    if @project.update_attributes(project_params)
      flash[:success] = "Project updated"
      redirect_to @project
    else
      @project.reload
      render 'show'
    end
  end

  def destroy
    project = Project.find(params[:id])
    project.update_attribute(:deleted, true)
    project.offers.update_all(deleted: true)
    flash[:success] = "Project deleted"
    redirect_to project.client
  end
  
  private

    def timelist_params
      if request.GET[:from].blank?
        @from = session[:date_from].try(:to_datetime) || Date.today - 2.weeks
      else
        @from = request.GET[:from].to_datetime
      end
      if request.GET[:to].blank?
        @to = session[:date_to].try(:to_datetime) || Date.today + 23.hours + 59.minutes
      else
        @to = request.GET[:to].to_datetime + 23.hours + 59.minutes
      end
      if request.GET[:notconf].blank?
        @notconf = false
      else
        @notconf = true
      end
      
      if request.GET['user'].blank?
        @user = session[:timelist_user] || 'all'
      else
        @user = request.GET['user']
      end

    end
    
    def project_params
      params.require(:project).permit(:name, :user_id, :csnumber, :client_id, :address, :status)
    end
end
