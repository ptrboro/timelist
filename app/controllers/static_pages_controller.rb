class StaticPagesController < ApplicationController
  before_action :only_logged_in, only: [:timelist, :drivebook]
  
  def home
    
    if logged_in?
      default_start = (Date.today + 7.hours)
      default_end = (Date.today + 15.hours)
      timelist_params
      @newRecord = WorkRecord.new(time_start: default_start, time_end: default_end)
      @user = current_user.id if !current_user.admin?
      
      project = @project == 'my' ? current_user.projects.map{ |p| p.id } : @project
      show_no_work = current_user.admin ? false : true
      @records = TimelistRecord.timelist(@from, @to, @user, project, show_no_work, @notconf).paginate(page: params[:page], per_page: 15).order(time_start: :desc, created_at: :desc)
      @nonWorkRecord = NoWorkRecord.new(record_type: 'work')
    end
  end
  
  
  def drivebook
  end
  
  private 
    def timelist_params
      if request.GET[:from].blank?
        @from = session[:date_from].try(:to_datetime) || Date.today - 2.weeks
      else
        @from = request.GET[:from].to_datetime
      end
      if request.GET[:to].blank?
        @to = session[:date_to].try(:to_datetime) || Date.today + 23.hours + 59.minutes
      else
        @to = request.GET[:to].to_datetime + 23.hours + 59.minutes
      end
      if request.GET[:notconf].blank?
        @notconf = false
      else
        @notconf = true
      end
      
      if request.GET['user'].blank?
        @user = session[:timelist_user] || 'all'
      else
        @user = request.GET['user']
      end
      
      if request.GET['project'].blank?
        @project = session[:timelist_project] || (current_user.admin ? 'my' : 'all')
      else
        @project = request.GET['project']
      end
      @user = current_user.id if !current_user.admin?
      store_timelist_data(@from, @to, @user, @project, @notconf)
    end
end
