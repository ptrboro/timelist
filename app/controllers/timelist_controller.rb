class TimelistController < ApplicationController
    
    before_action :only_logged_in
    before_action :check_authorization


    def update
       record = TimelistRecord.find(params[:id])
       if record.non_work_type.try(:title) == "Ferie" && record_params[:accepted]=="true"
           record.user.notifications.build(icon: "check-circle-o", title: "Ferie accepted", content: "Your ferie request (#{}-#{}) has been accepted", date: Date.today).save
       end
        if record.update_attributes(record_params)
          flash[:success] = "Record updated"
          redirect_back fallback_location: root_path
        else
          flash[:error] = "Record updating error"
          redirect_back fallback_location: root_path
        end
    end
    
    def destroy
        TimelistRecord.find(params[:id]).update_attribute(:deleted, true)
        flash[:success] = "Record deleted"
        redirect_back fallback_location: root_url
    end
    
    private
    
        def record_params
            if current_user.admin?
                params.require(:timelist_record).permit(:user_id,
                                               :non_work_type_id,
                                               :project_id,
                                               :time_start, :time_end, 
                                               :description, :accepted) 
            else
                bufor = params.require(:timelist_record).permit(:time_start, :time_end, 
                                               :non_work_type_id, :project_id,
                                               :description)
                                               
                bufor[:time_start] = bufor[:time_start].to_datetime
                bufor[:time_end] = bufor[:time_end].to_datetime
                bufor
            end
        end
        
        def check_authorization
            user = TimelistRecord.find(params[:id]).user
           redirect_back fallback_location: root_path unless current_user?(user) || current_user.admin?
        end
end