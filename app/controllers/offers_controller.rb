class OffersController < ApplicationController
    
    before_action :only_logged_in
    before_action :only_admin
    
    def index
        @offers = Offer.order(created_at: :desc).paginate(page: params[:page])
        @newOffer = Offer.new 
        @client = Client.new
        @admins = User.where(admin: true)
        @clients = Client.order(name: :asc)
        @projects = Project.order(name: :asc)
    end
    
    def show
        @offer = Offer.find(params[:id])
        @admins = User.where(admin: true)
        @clients = Client.order(name: :asc)
        @projects = Project.order(name: :asc)
        @newReminder = OfferReminder.new
        @newContact = ContactPerson.new
        @newContactRel = ContactRelation.new
    end
    
    def create
        @offers = Offer.order(created_at: :desc).paginate(page: params[:page])
        @admins = User.where(admin: true)
        @clients = Client.order(name: :asc)
        @projects = Project.order(name: :asc)
        @client = Client.new
        @newOffer = Offer.new(offer_params)
        
        if params[:offer][:client_id] == 'new'
            @client = Client.new(client_params)
            if @client.save
                @newOffer.client_id = @client.reload.id
            else
               render 'index' 
            end
        end
        
        
        if params[:offer][:project_id] == 'new'
            project = Project.new(name: @newOffer.name, client_id: @newOffer.client_id, status: @newOffer.status)
            if project.save
                @newOffer.project_id = project.reload.id
                if @newOffer.save
                    set_reminder if params[:offer][:set_reminder] == '1'
                    flash[:success] = "Offer and project created!"
                    redirect_to project
                else
                    project.destroy
                    @newOffer.errors[:base] << "Error"
                    render 'index'
                end
                
            else
                @newOffer.valid?
                render 'index'
            end
        else
            if @newOffer.save
                set_reminder if params[:offer][:set_reminder] == '1'
                flash[:success] = "Offer created!"
                redirect_to @newOffer
            else
                render 'index'
            end
        end
        
        
    end
    
    def update
        @offer = Offer.find(params[:id])
        if @offer.update_attributes(offer_params)
            flash[:success] = "Offer updated!"
            redirect_to @offer
        else
            @admins = User.where(admin: true)
            @clients = Client.order(name: :asc)
            @projects = Project.order(name: :asc)
            @newReminder = OfferReminder.new
            @newContact = ContactPerson.new
            @newContactRel = ContactRelation.new
            @offer.reload
            render 'show'
        end
    end
    
    def destroy
        @offer = Offer.find(params[:id])
        @offer.destroy                  #usuń ofertę
        @offer.reminders.destroy_all    #usuń przypomnienia
        @offer.contacts.destroy_all     #usuń powiązania z kontaktami
        flash[:success] = "Offer deleted."
        redirect_to @offer.project
    end
    
    private
        def offer_params
           params.require(:offer).permit(:name, 
                                        :identifier, 
                                        :client_id, 
                                        :project_id, 
                                        :user_id, 
                                        :status,
                                        :deadline,
                                        :start_date,
                                        :description)
        end
        
        def client_params
            params.require(:client).permit(:name)
        end
        
        def set_reminder
            days_difference = (@newOffer.deadline - Date.today).to_i
            date = @newOffer.deadline - 60.days  if days_difference > 180
            date = @newOffer.deadline - 30.days  if days_difference.between?(60, 180)
            date = @newOffer.deadline - 14.days  if days_difference.between?(30, 59)
            date = @newOffer.deadline - 7.days  if days_difference.between?(14, 29)
            date = @newOffer.deadline - 2.days  if days_difference.between?(4, 13)
            date = @newOffer.deadline - 1.day  if days_difference.between?(2, 3)
            return false if days_difference < 2
            
            reminder = @newOffer.reminders.build(message: "Check this offer",
                                          date: date,
                                          email: true)
            reminder.save
        end
end
