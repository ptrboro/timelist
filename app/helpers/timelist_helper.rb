module TimelistHelper
    def timelist(from, to, user_id, project_id, notconf, page)
        work_records = WorkRecord.timelist(from, to, user_id, project_id, notconf)
        non_work_records = NonWorkRecord.timelist(from, to, user_id, 'all', notconf)
        list = []
        work_records.each do |record|
            list << record
        end
        non_work_records.each do |record|
            list << record
        end
        
        list.sort! { |a,b| a.time_start <=> b.time_start }
        session[:timelist_pagination] = list.count % 20 == 0 ? (list.count/20) : ((list.count/20) +1)
        
        if list[(page*20-20)..(page*20)].nil?
            list = []
        else 
            list[(page*20-20)..(page*20)]
        end
    end
end