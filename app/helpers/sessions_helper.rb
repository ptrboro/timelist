module SessionsHelper
    
    # Logs in the given user
    def log_in(user)
        session[:user_id] = user.id
    end
    
    def remember(user)
       user.remember
       cookies.permanent.signed[:user_id] = user.id
       cookies.permanent[:remember_token] = user.remember_token
    end
    
    
    # Returns current logged-in user (if any)
    def current_user
        if (user_id = session[:user_id])
            @current_user ||= User.find_by(id: user_id)
        elsif (user_id = cookies.signed[:user_id])
            user = User.find_by(id: user_id)
            if user && user.authenticated?(cookies[:remember_token])
                log_in user
                @current_user = user
            end
        end

    end
    
    # returns true if given user is a current user
    def current_user?(user)
       user == current_user
    end
    
    # Returns True if user is logged in, false if not
    def logged_in?
        !current_user.nil?
    end
    
    def forget(user)
        user.forget
        cookies.delete(:user_id)
        cookies.delete(:remember_token)
    end
    
    # Logs out the current user.
    def log_out
        forget(current_user)
        forget_timelist_data
        session.delete(:user_id)
        @current_user = nil
    end
    
    def redirect_back_or(default)
       redirect_to(session[:forwarding_url] || default)
       session.delete(:forwarding_url)
    end
    
    def store_location
        session[:forwarding_url] = request.original_url if request.get? 
    end
    
    def store_timelist_data(date_from = nil, date_to = nil, user_id = nil, project_id = nil, notconf = nil)
        session[:date_from] = date_from || Date.today-2.weeks
        session[:date_to] = date_to || Date.today + 23.hours + 59.minutes
        session[:timelist_user] = user_id || 'all'
        session[:timelist_project] = project_id || 'all'
        session[:notconf] = notconf || 'false'
    end
    
    def forget_timelist_data
       session.delete(:date_from)
       session.delete(:date_to)
       session.delete(:timelist_user)
       session.delete(:timelist_project)
       session.delete(:notconf)
    end
end
