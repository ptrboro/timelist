# id
# user_id           :integer
# record_type       :string  <work>
# project_id        :integer
# time_start        :datetime
# time_end          :datetime
# hours             :text     < [ord:4.5][p50:3.5][p100:1.0][extra:4.5] itp.
# description       :text
# accepted          :boolean, false
# deleted           :boolean, false


class WorkRecord < TimelistRecord
    
    default_scope { where(record_type: 'work') }
    validates_associated :project
    
    # def initialize
    #    super
    #    self.record_type = 'no_work'
    # end
end

