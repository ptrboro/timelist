class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  default_scope { where(deleted: false) }
  
  def destroy
    update_attribute(:deleted, true)
  end
  def destroy_all
    update_all(deleted: true)
  end
end
