class Notification < ApplicationRecord
  belongs_to :user
  default_scope { where(seen: false).where("date <= ?", Date.today) }
  
  def Notification.build_notifications(date = Date.today)
      
      OfferReminder.where(date: date).each do |reminder|
         notification = Notification.new(icon: "bell-o",
                                         title: "Reminder",
                                         content: reminder.message,
                                         user_id: reminder.offer.user.id,
                                         date: reminder.date,
                                         content_id: reminder.offer.id,
                                         content_type: "offer")
        notification.save
      end
      Offer.where(deadline: date).each do |offer|
         notification = Notification.new(icon: "clock-o",
                                         title: "Deadline",
                                         content: "Today is deadline for #{offer.name}",
                                         user_id: offer.user.id,
                                         date: offer.deadline,
                                         content_id: offer.id,
                                         content_type: "offer")
         notification.save
      end
  end
  
  def Notification.del_all
     unscoped.all.each do |n|
         n.destroy
     end
  end
end
