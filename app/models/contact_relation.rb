class ContactRelation < ApplicationRecord
    
    belongs_to :offer
    belongs_to :contact_person
    
    validates :offer_id, presence: true
    validates :contact_person_id, presence: true
    
end
