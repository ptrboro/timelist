class ContactPerson < ApplicationRecord
    
    # id, offer_id, tel, email, occupation, description, deleted
    
  belongs_to :client
  validates :name, presence: true
  validates :client_id, presence: true 
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email,   length: { maximum: 255 }, 
                      # format: { with: VALID_EMAIL_REGEX },
                      uniqueness: {case_sensitive: false },
                      allow_nil: true
                      
  before_save { email.downcase! }
  
end
