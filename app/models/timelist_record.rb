# id
# user_id           :integer
# record_type       :string  <work / no_work >
# project_id        :integer
# non_work_type_id  :integer
# time_start        :datetime
# time_end          :datetime
# hours             :text     < hours_ord=4.5&hours_50=3.5&hours_extra=4.5 > itp.
# description       :text
# accepted          :boolean, false
# deleted           :boolean, false


class TimelistRecord < ApplicationRecord
    
    belongs_to :user
    belongs_to :project, optional: true
    belongs_to :non_work_type, optional: true
    validates_associated :user
    validates :record_type, presence:true, inclusion: { in: %w(work no_work), message: "%{value} is not a valid type" }
    validates_datetime :time_start, message: 'Wrong start time format'
    validates_datetime :time_end, message: 'Wrong end time format'
    validate :time_end_is_after_time_start
    before_save :set_hours
    
    
     
    def self.timelist(from, to, user_id, project_id, show_no_work, notconf)
        query_hash = {}

        query_hash[:accepted] = false if notconf
        query_hash[:user_id] = user_id if user_id != 'all'
        query_hash[:project_id] = project_id if project_id != 'all'
        query_hash[:record_type] = 'work' unless show_no_work
        where(query_hash).where('time_start <= ? AND time_end >= ?', to, from)
    end
    
    # zwraca hash godzin dla danego rekordu
    def hours_hash
        self.hours.split('&').map{|h| h1,h2 = h.split('='); {h1 => h2}}.reduce(:merge)
    end
    
    # Zlicza godziny na podstawie ustawionych czasów startu i końca
    # Praca w dni zwykłe - pierwsze 8 godzin jako normalne,
    #                      kolejne 3 godziny jako premiowane 50%,
    #                      następne godziny jako premiowane 100%,
    #                      - 0.5 godziny niepłatnej przerwy (po 4 godz.)
    #
    # Praca w sobotę -     pierwsze 5 godzin jako premiowane 50%,
    #                      kolejne jako premiowane 100%
    #
    # Praca w niedzielę -  każda godzina premium 100%
    #
    # Praca po 20:00    -  zawsze 100% (nie działa jeszcze)
    #
    # CO JEŚLI SĄ DWA REKORDY TEGO SAMEGO DNIA?
    
    def count_hours
        
        h = (self.time_end-self.time_start)/3600
        week_day = self.time_start.wday
        hours_hash = {}
        
        # praca w sobotę
        if week_day == 6
            if h <= 5
              hours_hash[:hours_50]  = h
            elsif h > 5
              hours_hash[:hours_50]  = 5
              hours_hash[:hours_100] = h - 5
            end
            
        # praca w niedzielę
        elsif week_day == 7
            hours_hash[:hours_100] = h
            
        # praca w tygodniu
        else
            if h <= 4
              hours_hash[:hours_ord] = h
            elsif h > 4 && h <= 8
              hours_hash[:hours_ord] = h - 0.5 # when working more than 4h - pause is not paid
            elsif h > 8 && h <= 11
              hours_hash[:hours_ord] = 7.5
              hours_hash[:hours_50]  = h - 8
            elsif h > 11
              hours_hash[:hours_ord] = 7.5
              hours_hash[:hours_50]  = 3
              hours_hash[:hours_100] = h - 11.5
            end
        end
        hours_hash
    end
    
    private
        # VALIDATOR   czas początkowy po czasie końca rekordu
        def time_end_is_after_time_start
          return if time_start.blank? || time_end.blank?
          if time_end < time_start
            errors.add(:time_end, "cannot be before the start time") 
          end 
        end
        
        # przed zapisem przekształca hash godzin na format stringowy
        def set_hours
          return if self.record_type == "no_work"
           self.hours = self.count_hours.map{|k,v| "#{k}=#{v}"}.join('&') 
        end
        
        
end
