class Project < ApplicationRecord
  belongs_to :client
  belongs_to :user
  has_many :offers
  
  validates :name, presence: true
  validates :client_id, presence: true
  
  def should_be_updated?
    self.address.blank? || self.csnumber.blank? || self.status.blank? 
  end
end
