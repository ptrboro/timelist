class TimelineElement
    
    attr_accessor :status
    attr_accessor :icon
    attr_accessor :title
    attr_accessor :date
    attr_accessor :created_by
    attr_accessor :content
    attr_accessor :source
    
    def initialize(icon, title, date, created_by, content, source)
        @icon = icon
        @title = title
        @date = date
        @created_by = created_by
        @content = content
        @source = source
        
       @status = "past" if date < Date.today
       @status = "today" if date == Date.today
       @status = "future" if date > Date.today
    end
    
end


class Offer < ApplicationRecord
    before_create :generate_identifier
    after_create :build_notification
    after_update :update_notification
    
    belongs_to :client
    belongs_to :project
    belongs_to :user, -> { where(admin: true) }
    has_many :reminders, class_name: "OfferReminder"
    
    
    has_many :contacts, class_name: "ContactRelation"
    has_many :contact_people, through: :contacts, source: :contact_person
    
    attr_accessor :set_reminder #Without this checkbox in form was broken
    
    validates :name, presence: true
    validates :client, presence: true
    validates :status, inclusion: { in: ["pending", "in progress", "finished"],
    message: "%{value} is not a valid status" }
    
    def timeline
        timeline = []
        # ADD CREATED AT ITEM
        timeline << TimelineElement.new("first-order",
                                    "Offer created",
                                    self.created_at,
                                    nil, nil, nil)

        # DODAJ REMINDERY
        self.reminders.each do |reminder|
            timeline << TimelineElement.new("bell-o",
                                    "Reminder",
                                    reminder.date,
                                    nil,
                                    reminder.message,
                                    reminder)
        end
        
        timeline << TimelineElement.new("reply",
                                    "Today",
                                    Date.today,
                                    nil,
                                    nil,
                                    nil)
                                    
        # DODAJ DZISIEJSZY DZIEŃ
        if self.deadline 
             timeline << TimelineElement.new("first-order",
                                    "Deadline",
                                    self.deadline,
                                    nil,
                                    "Last day to send offer response",
                                    nil)
        end
        if self.start_date
            timeline << TimelineElement.new("first-order",
                                    "Project start",
                                    self.start_date,
                                    nil,
                                    "Project start on this day",
                                    nil)
        end
        timeline.sort! { |a,b| a.date <=> b.date }
    end
    
    private
        def generate_identifier
            id = Offer.last.try(:identifier) || "0/0"
            id = id.split('/')
            id = id[0].to_i
            id = id + 1
            id = id.to_s + '/' + Time.now.year.to_s
            self.identifier = id
        end
        
        def build_notification
            if self.deadline
                notification = Notification.new(icon: "clock-o",
                                                title: "Deadline",
                                                content: "Deadline for #{self.name} is today",
                                                user_id: self.user.id,
                                                date: self.deadline,
                                                content_id: self.id,
                                                content_type: "offer")
                notification.save
            end
            if self.start_date
                notification = Notification.new(icon: "clock-o",
                                                title: "Offer start",
                                                content: "#{self.name} starts today",
                                                user_id: self.user.id,
                                                date: self.start_date,
                                                content_id: self.id,
                                                content_type: "offer")
                notification.save
            end
        end
        def update_notification
          if notification = Notification.unscoped.find_by(title: "Deadline", content_type: "offer", content_id: self.id)
              self.deadline >= Date.today ? seen = false : seen = true
              notification.update_attributes(user_id: self.user.id,
                                             date: self.deadline,
                                             seen: seen)
          end
          if notification = Notification.unscoped.find_by(title: "Offer start", content_type: "offer", content_id: self.id)
              self.start_date >= Date.today ? seen = false : seen = true
              notification.update_attributes(user_id: self.user.id,
                                             date: self.start_date,
                                             seen: seen)
          end
        end
end
