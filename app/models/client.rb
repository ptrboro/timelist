class Client < ApplicationRecord
    has_many :projects
    has_many :offers
    has_many :contact_people
    
    validates :name, presence: true
end
