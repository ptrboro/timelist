class NonWorkType < ApplicationRecord
    has_many :non_work_record
    validates :title, presence: true
end
