class OfferReminder < ApplicationRecord
  belongs_to :offer
  after_create :build_notification
  after_update :update_notification
  
  validates :message, presence: true
  validates :date, presence: true
  validates :offer_id, presence: true
  
  private 
    def build_notification
      notification = Notification.new(icon: "bell-o",
                                           title: "Reminder",
                                           content: self.message,
                                           user_id: self.offer.user.id,
                                           date: self.date,
                                           content_id: self.offer.id,
                                           content_type: "offer")
      notification.save
    end
    def update_notification
      if notification = Notification.unscoped.find_by(title: "Reminder", content_type: "offer", content_id: self.id)
        self.date >= Date.today ? seen = false : seen = true
        notification.update_attributes(content: self.message,
                                       user_id: self.offer.user.id,
                                       date: self.date,
                                       seen: seen)
      end
    end
  
  
end
