class StatusUpdateWorker
  include Sidekiq::Worker

  def perform
      Offer.where(status: "pending", start_date: Date.today).each do |offer|
         offer.update_attribute(:status, "in progress")
      end
  end
end