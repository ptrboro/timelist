require 'test_helper'

class ProjectsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @admin = users(:admin_user)
    @user = users(:mark)
    @project = projects(:strommen)
  end
  
  test "should get index" do
    log_in_as(@admin)
    get projects_url
    assert_response :success
  end

  test "should get show" do
    log_in_as(@admin)
    get project_url(@project)
    assert_response :success
  end
  
  test "non admin users should be redirected" do
    log_in_as(@user)
    get projects_url
    assert_redirected_to root_url
    get project_url(@project)
    assert_redirected_to root_url
    patch project_url(@project), params: { project: { name: "xxxx",
                                                      csnumber: 140 } }
    assert_redirected_to root_url
  end
end
