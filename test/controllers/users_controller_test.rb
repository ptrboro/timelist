require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:mark)
    @other_user = users(:archer)
    @admin = users(:admin_user)
  end
  
  test "should get index" do
    log_in_as(@admin)
    get users_path
    assert_response :success
  end
  
  test "should redirect when non logged in" do
    get users_path
    assert_redirected_to root_path
    get user_path(@user)
    assert_redirected_to root_path
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email } }
    assert_redirected_to root_path
  end
  
  test "should redirect update when log in as a wrong user" do
    log_in_as(@other_user)
    patch user_path(@user), params: { user: { name: "Testoviron Test",
                                              email: "test@example.com" } }
    assert_redirected_to root_path
  end
  
  test "admin should be able to show and edit other users" do
    log_in_as(@admin)
    # show user page
    get user_path(@user)
    assert_template 'users/show'
    
    # edit user
    patch user_path(@user), params: { user: { name: "Testoviron Test",
                                              email: "test@testoviron.com" } }
    assert_redirected_to @user
  end
  
  test "non admin users can not edit their rights" do
    log_in_as(@user)
    assert_not @user.admin?
    patch user_path(@user), params: { user: { password: "password",
                                              password_confirmation: "password",
                                              admin: true } }
    assert_not @user.reload.admin?
  end
  
  test "admin users can edit other users to be admins" do
    log_in_as(@admin)
    assert_not @user.admin?
    patch user_path(@user), params: { user: { password: "password",
                                              password_confirmation: "password",
                                              admin: true } }
    assert @user.reload.admin?
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to root_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@user)
    assert_no_difference 'User.count' do
      delete user_path(@other_user)
    end
    assert_redirected_to root_url
  end
  
end
