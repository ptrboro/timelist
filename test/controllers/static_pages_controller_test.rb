require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @admin = users(:admin_user)
    @user = users(:mark)
  end
  
  test "non logged in users should be redirected" do
     get timelist_url
    assert_redirected_to root_path
     get drivebook_url
    assert_redirected_to root_path
  end

  
  test "should get timelist" do
    log_in_as @user
    get timelist_path
    assert_response :success
    assert_select "title", "Timelist | Christiania Stillas Timelist"
  end
  
  test "should get drivebook" do
    log_in_as @user
    get drivebook_path
    assert_response :success
    assert_select "title", "Drivebook | Christiania Stillas Timelist"
  end
end
