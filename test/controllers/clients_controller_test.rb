require 'test_helper'

class ClientsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @client = clients(:backe)
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'Client.count' do
      post clients_path, params: { client: { name: "Test Client", 
                                             address: "Truskawkowa 15, Oslo 9999" 
                                  } }
    end
    assert_redirected_to root_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'Client.count' do
      delete client_path(@client)
    end
    assert_redirected_to root_path
  end
end
