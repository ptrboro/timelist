require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  
  def setup
    @admin = users(:admin_user)
  end
  
  test "invalid add user information" do
    log_in_as(@admin)
    get users_path
    assert_no_difference 'User.count' do
    post users_path, params: { user: { name: "",
                                         email: "invalid@goo." } }
    end
    assert_template 'users/index'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end
  
  test "valid add user information" do
    log_in_as(@admin)
    get users_path
    assert_difference 'User.count', 1 do
    post users_path, params: { user: { name: "Example user",
                                         email: "example@example.com" } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert_select 'div#flash'
  end
  
end
