require 'test_helper'
class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  test "should get login form" do
    get root_path
    assert_select 'form.form-inline'
  end
  
  test "layout links" do
    user = users(:mark)
    log_in_as(user)
    follow_redirect!
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
  end
  
end
