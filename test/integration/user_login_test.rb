require 'test_helper'

class UserLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:mark)
  end
  
  test "login with invalid informations" do
    get root_path
    assert_template 'static_pages/home'
    post root_path, params: { session: { email: "invalid@email.co",
                                          password: "invalid" } }
    assert_template 'static_pages/home'
    assert flash.any?
    get root_url
    assert flash.empty?
  end
  
  test "login with valid informations" do
    get root_path
    assert_template 'static_pages/home'
    post root_path, params: { session: { email: @user.email,
                                          password: "password" } }
    follow_redirect!
    assert_template 'static_pages/home'
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    assert is_logged_in?
  end
  
  test "logout should work" do
    log_in_as(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_path
    follow_redirect!
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end
  
  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_not_nil cookies['remember_token']
  end
  
  test "login without remembering" do
    log_in_as(@user, remember_me: '0')
    assert_nil cookies['remember_token']
  end
end
