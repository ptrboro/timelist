require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:mark)
    @admin_user = users(:admin_user)
  end
  
  test "non admin users should be redirected to root url" do
    log_in_as(@user)
    get users_path
    assert_redirected_to root_path
  end 
  
  test "index should be paginated and include delete buttons" do
   log_in_as(@admin_user)
   get users_path
   assert_template 'users/index'
   assert_select 'div.pagination'
   User.paginate(page: 1).each do |user|
     assert_select 'a[href=?]', user_path(user), text: user.name
     assert_select 'form[action=?]', user_path(user)
   end
  end
end
