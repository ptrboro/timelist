require 'test_helper'

class UserEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:mark)
  end
  
  test "unsuccessful edit" do
    log_in_as(@user)
    get user_path(@user)
    patch user_path(@user), params: { user: { name: "",
                                              email: "invalid@foo",
                                              password: "foo",
                                              password_confirmation: "bar" } }
    assert_select 'div#error_explanation'
  end
  
  test "successful edit" do
    log_in_as(@user)
    get user_path(@user)
    name = "Testoviron Test"
    email = "test@examlpea.pl"
    patch user_path(@user), params: { user: { name: name,
                                              email: email,
                                              password: "",
                                              password_confirmation: "" } }
    assert_redirected_to @user
    follow_redirect!
    assert_not flash.empty?
    assert_select 'div#error_explanation', count: 0
    assert_template 'users/show'
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
  
  
  
  test "friendly forwarding" do
    get user_path(@user)
    assert_redirected_to root_path
    log_in_as(@user)
    assert_redirected_to user_path(@user)
  end
  
  
end
