require 'test_helper'

class ClientTest < ActiveSupport::TestCase
  
  def setup
    @client = Client.new(name: "Bake", address: "Kwiatkowa 12, 2099 Oslo")
  end
  
  test "should be valid" do
    assert @client.valid?
  end
  
  test "name should be present" do
    @client.name = "    "
    assert_not @client.valid?
  end
  
end
