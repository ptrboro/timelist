require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  def setup
    @client = clients(:backe)
    @project = @client.projects.build(name: "Strommen Center",
                           csnumber: 123,
                           address: "Strommen 17, 2010 Lillestrom",
                           status: "in progress")
  end
  
  test "should be valid" do
    assert @project.valid?
  end
  
  test "name should be present" do
    @project.name = "     "
    assert_not @project.valid?
  end
  
  test "client id should be present" do
    @project.client_id = nil
    assert_not @project.valid?
  end
end
