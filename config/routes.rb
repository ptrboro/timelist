Rails.application.routes.draw do
  get 'projects/index'

  get 'projects/show'

  get 'projects/create'

  get 'projects/update'

  get 'projects/destroy'

  get 'dashboard', to: 'timelist#dashboard'

  root 'static_pages#home'
  get '/timelist', to: 'static_pages#timelist'
  get '/drivebook', to: 'static_pages#drivebook'
  post '/', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  
  resources :users, only: [:show, :index, :destroy, :create, :update]
  resources :clients, only: [:show, :index, :destroy, :create, :update]
  resources :projects, only: [:show, :index, :destroy, :create, :update]
  resources :offers, only: [:show, :index, :destroy, :create, :update]
  resources :offer_reminders, only: [:create, :destroy, :update]
  resources :contact_people, only: [:create, :destroy, :update]
  resources :notifications, only: [:show]
  resources :timelist_records, only: [:create, :destroy, :update], controller: :timelist
  resources :no_work_records, only: [:create, :destroy, :update]
  resources :work_records, only: [:create, :destroy, :update]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
