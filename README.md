# Ruby on Rails Timelist project
This is the application for tracking working time.
Scaffolders.

## License

Piotr Borowiec. All rights reserved.

## Git Repository

https://ptrboro@bitbucket.org/ptrboro/timelist.git

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Seed the database:

```
$ rails db:seed
```

Finally, run the app in a local server:

```
$ rails server
```