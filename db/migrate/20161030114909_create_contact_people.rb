class CreateContactPeople < ActiveRecord::Migration[5.0]
  def change
    create_table :contact_people do |t|
      t.string :name
      t.references :offer, foreign_key: true
      t.text :tel
      t.text :email
      t.string :occupation
      t.text :description
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
