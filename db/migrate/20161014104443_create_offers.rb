class CreateOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :offers do |t|
      t.string :name
      t.string :identifier, unique: true
      t.belongs_to :client, foreign_key: true, index: true
      t.belongs_to :project, foreign_key: true, index: true
      t.belongs_to :user, foreign_key: true, index: true
      t.string :status
      t.date :start_date
      t.text :notes
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
