class CreateNonWorkTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :non_work_types do |t|
      t.string :title
      t.text :instructions
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
