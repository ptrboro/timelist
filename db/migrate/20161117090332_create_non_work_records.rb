class CreateNonWorkRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :non_work_records do |t|
      t.references :user, foreign_key: true
      t.references :non_work_type, foreign_key: true
      t.datetime :time_start
      t.datetime :time_end
      t.decimal :hours_ord
      t.decimal :hours_50
      t.decimal :hours_100
      t.decimal :hours_extra
      t.decimal :hours_bank
      t.text :description
      t.boolean :accepted, default: false
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
