class AddDateToOfferReminders < ActiveRecord::Migration[5.0]
  def change
    change_column :offer_reminders, :date,  :date
  end
end
