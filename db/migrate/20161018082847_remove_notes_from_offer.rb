class RemoveNotesFromOffer < ActiveRecord::Migration[5.0]
  def up
    remove_column :offers, :notes
  end

  def down
    add_column :offers, :notes, :text
  end
end
