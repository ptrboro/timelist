class CreateOfferReminders < ActiveRecord::Migration[5.0]
  def change
    create_table :offer_reminders do |t|
      t.text :message
      t.datetime :date
      t.boolean :email, default: false
      t.references :offer, foreign_key: true, index: true
      t.boolean :deleted, default:false

      t.timestamps
    end
  end
end
