class CreateTimelistRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :timelist_records do |t|
      t.references :user, foreign_key: true
      t.string :type
      t.references :project, foreign_key: true
      t.references :non_work_type, foreign_key: true
      t.datetime :time_start
      t.datetime :time_end
      t.text :hours
      t.text :description
      t.boolean :accepted, default: false
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
