class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.references :client, foreign_key: true
      t.integer :csnumber
      t.text :address
      t.string :status
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
