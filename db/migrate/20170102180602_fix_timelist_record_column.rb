class FixTimelistRecordColumn < ActiveRecord::Migration[5.0]
  def change
    rename_column :timelist_records, :type, :record_type
  end
end
