class RemoveOfferIdFromContactPeople < ActiveRecord::Migration[5.0]
  def up
    remove_column :contact_people, :offer_id, :integer
    add_column :contact_people, :client_id, :integer
  end
  
  def down
    add_column :contact_people, :offer_id, :integer
    remove_column :contact_people, :client_id, :integer
  end
end
