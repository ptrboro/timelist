class AddActiveToClients < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :active, :boolean, default: true
    add_column :clients, :deleted, :boolean, default: false
  end
end
