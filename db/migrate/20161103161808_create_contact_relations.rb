class CreateContactRelations < ActiveRecord::Migration[5.0]
  def change
    create_table :contact_relations do |t|
      t.integer :offer_id
      t.integer :contact_person_id

      t.timestamps
    end
    
    add_index :contact_relations, :offer_id
    add_index :contact_relations, :contact_person_id
    add_index :contact_relations, [:offer_id, :contact_person_id], unique: true
  end
end
