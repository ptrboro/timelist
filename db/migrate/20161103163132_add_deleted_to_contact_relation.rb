class AddDeletedToContactRelation < ActiveRecord::Migration[5.0]
  def change
    add_column :contact_relations, :deleted, :boolean, default: false
  end
end
