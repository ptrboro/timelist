class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.string :icon
      t.string :title
      t.text :content
      t.date :date
      t.references :user, foreign_key: true
      t.integer :content_id
      t.string :content_type
      t.boolean :seen, default: false
      t.boolean :mailing, default: false
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
