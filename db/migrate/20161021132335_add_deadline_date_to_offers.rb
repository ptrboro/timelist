class AddDeadlineDateToOffers < ActiveRecord::Migration[5.0]
  def change
    add_column :offers, :deadline, :date
  end
end
