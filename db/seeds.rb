# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# standard admin account, email: jsmith@timelist.com
User.create!(name: "John Smith",
             email: "jsmith@timelist.com",
             password: "password",
             password_confirmation: "password",
             address: "Warsaw, Mokotów",
             admin: true)

# standard user account, email: mj@timelist.com
User.create!(name: "Michael Johnson",
             email: "mj@timelist.com",
             password: "password",
             password_confirmation: "password",
             address: "Warsaw, Mokotów",
             admin: false)
             
User.create!(name: "Piotr Borowiec",
             email: "p.borowiec01@gmail.com",
             password: "password",
             password_confirmation: "password",
             address: "Częstochowa, relocation to Warsaw",
             admin: true)

40.times do |n|
    name = Faker::Name.name
    email = "example-#{n+1}@timelist.org"
    address = Faker::Address.street_address + ", " + Faker::Address.city
    password = "password"
    User.create!(name: name,
                 email: email,
                 address: address,
                 password: password,
                 password_confirmation: password)
end

15.times do |n|
   name = Faker::Company.name
   address = Faker::Address.street_address + ", " + Faker::Address.city
   Client.create!(name: name,
                  address: address)
end

clients = Client.order(:name).take(6)

clients.each do |client|
   4.times do |n|
      name = Faker::Address.street_name + "Project_#{n}"
      address = Faker::Address.street_address + ", " + Faker::Address.city
      user = User.where(admin:true).first
      client.projects.build(name: name,
                            user: user,
                            csnumber: 1000+n,
                            address: address,
                            status: 'in progress').save!
                            
   end
end

projects = Project.order(:name).take(5)

projects.each do |project|
   3.times do |n|
       name = "Offer #{n}"
       client = project.client
       user = User.where(admin: true).first
       status = "pending"
       project.offers.build(name: name,
                            client: client,
                            user: user,
                            status: status).save!
   end
end

User.where(admin:false).take(10).each do |user|
    
    20.times do |n|
        project = Project.find(n/6 + 2)
        time_start = Date.today - n.days + 7.hours
        time_end = Date.today - n.days + (15 + n % 4).hours
        user.work_records.build(project: project,
                                time_start: time_start,
                                time_end: time_end).save!
    end
end