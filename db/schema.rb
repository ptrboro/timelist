# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170102180602) do

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.text     "address"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "active",     default: true
    t.boolean  "deleted",    default: false
  end

  create_table "contact_people", force: :cascade do |t|
    t.string   "name"
    t.text     "tel"
    t.text     "email"
    t.string   "occupation"
    t.text     "description"
    t.boolean  "deleted",     default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "client_id"
  end

  create_table "contact_relations", force: :cascade do |t|
    t.integer  "offer_id"
    t.integer  "contact_person_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.boolean  "deleted",           default: false
    t.index ["contact_person_id"], name: "index_contact_relations_on_contact_person_id"
    t.index ["offer_id", "contact_person_id"], name: "index_contact_relations_on_offer_id_and_contact_person_id", unique: true
    t.index ["offer_id"], name: "index_contact_relations_on_offer_id"
  end

  create_table "non_work_records", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "non_work_type_id"
    t.datetime "time_start"
    t.datetime "time_end"
    t.decimal  "hours_ord"
    t.decimal  "hours_50"
    t.decimal  "hours_100"
    t.decimal  "hours_extra"
    t.decimal  "hours_bank"
    t.text     "description"
    t.boolean  "accepted",         default: false
    t.boolean  "deleted",          default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["non_work_type_id"], name: "index_non_work_records_on_non_work_type_id"
    t.index ["user_id"], name: "index_non_work_records_on_user_id"
  end

  create_table "non_work_types", force: :cascade do |t|
    t.string   "title"
    t.text     "instructions"
    t.boolean  "deleted",      default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.string   "icon"
    t.string   "title"
    t.text     "content"
    t.date     "date"
    t.integer  "user_id"
    t.integer  "content_id"
    t.string   "content_type"
    t.boolean  "seen",         default: false
    t.boolean  "mailing",      default: false
    t.boolean  "deleted",      default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "offer_reminders", force: :cascade do |t|
    t.text     "message"
    t.date     "date"
    t.boolean  "email",      default: false
    t.integer  "offer_id"
    t.boolean  "deleted",    default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["offer_id"], name: "index_offer_reminders_on_offer_id"
  end

  create_table "offers", force: :cascade do |t|
    t.string   "name"
    t.string   "identifier"
    t.integer  "client_id"
    t.integer  "project_id"
    t.integer  "user_id"
    t.string   "status"
    t.date     "start_date"
    t.boolean  "deleted",     default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.text     "description"
    t.date     "deadline"
    t.index ["client_id"], name: "index_offers_on_client_id"
    t.index ["project_id"], name: "index_offers_on_project_id"
    t.index ["user_id"], name: "index_offers_on_user_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.integer  "client_id"
    t.integer  "csnumber"
    t.text     "address"
    t.string   "status"
    t.boolean  "deleted",    default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "user_id"
    t.index ["client_id"], name: "index_projects_on_client_id"
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "timelist_records", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "record_type"
    t.integer  "project_id"
    t.integer  "non_work_type_id"
    t.datetime "time_start"
    t.datetime "time_end"
    t.text     "hours"
    t.text     "description"
    t.boolean  "accepted",         default: false
    t.boolean  "deleted",          default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["non_work_type_id"], name: "index_timelist_records_on_non_work_type_id"
    t.index ["project_id"], name: "index_timelist_records_on_project_id"
    t.index ["user_id"], name: "index_timelist_records_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.string   "address"
    t.string   "remember_digest"
    t.boolean  "admin",           default: false
    t.boolean  "deleted",         default: false
    t.string   "occupation"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "work_records", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "project_id"
    t.datetime "time_start"
    t.datetime "time_end"
    t.decimal  "hours_ord"
    t.decimal  "hours_50"
    t.decimal  "hours_100"
    t.decimal  "hours_extra"
    t.decimal  "hours_bank"
    t.text     "description"
    t.boolean  "accepted",    default: false
    t.boolean  "deleted",     default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["project_id"], name: "index_work_records_on_project_id"
    t.index ["user_id"], name: "index_work_records_on_user_id"
  end

end
